package edu.asu.emit.algorithm.graph;

import edu.asu.emit.algorithm.graph.shortestpaths.YenTopKShortestPathsAlg;
import edu.asu.emit.algorithm.utils.Pair;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by vishnu on 4/27/16.
 */
public class RoutingAndWavelengthAssignment {
    private boolean pathFound=false;
    private Graph Topology=null;
    private Requests RequestSet=null;
    private YenTopKShortestPathsAlg yenAlg=null;
    private int start=0;
    private int end=0;
    public RoutingAndWavelengthAssignment(Graph graph,Requests requests){
        this.Topology=graph;
        this.RequestSet=requests;
        yenAlg = new YenTopKShortestPathsAlg(graph);
    }
//    YenTopKShortestPathsAlg yenAlg = new YenTopKShortestPathsAlg(graph);
    public void FirstFitAssignment(int MaxWavelengths){
        int waveLengthLabel=1;
        ArrayList<ArrayList<Path>> WavelengthList = new ArrayList<ArrayList<Path>>();
        for(int count=0;count<MaxWavelengths;count++){
            ArrayList<Path>wavelength=new ArrayList<Path>();
            WavelengthList.add(wavelength);
        }

//        System.out.println(MaxWavelengths);
        for (int i=0;i<RequestSet.length();i++) {

            waveLengthLabel=1;
            edu.asu.emit.algorithm.utils.Pair<Integer, Integer> CurrentDemand=RequestSet.Demand.get(i + 1);
            start=CurrentDemand.first();
            end=CurrentDemand.second();
            System.out.println("\n Demand:"+(i+1)+" between "+ start+" and "+end);
            List<Path> shortest_paths_list = yenAlg.getShortestPaths(
                    Topology.getVertex(start), Topology.getVertex(end), 100);
            pathFound=false;
//            System.out.println(":" + shortest_paths_list+" "+shortest_paths_list.size());
            for(Path path:shortest_paths_list){//iterating through shortest paths
                waveLengthLabel=1;
                for(ArrayList<Path> wavelength:WavelengthList){// Iterating through available wavelengths
                    pathFound=false;
                    if(wavelength.size()==0){//If no paths are assigned for a wavelength, assign a path for the wavelength
                            pathFound=true;
                            System.out.println("Path "+path+" is chosen and wavelength λ"+waveLengthLabel+ " is assigned" );
                            wavelength.add(path);
                            break;
                        }
                            pathFound=true;
                        for(Path currentPath: wavelength){//Checking if any paths assigned to the wavelength shares an edge with the newly computed path
                            if(currentPath.checkOverlappingEdges(path)){
                                pathFound=false;//if edge is shared, abort
                                break;
                            }
                        }
                    if(pathFound==true)//IF all the paths assigned to the wavelength doesn't contain an edge sharing with the new path, assign the new path to wavelength
                        {
                            System.out.println("Path "+path+" is chosen and wavelength λ"+waveLengthLabel+ " is assigned" );
                            wavelength.add(path);
                            break;
                        }
                   waveLengthLabel++;//Incrementing wavelength label
                }
                if(pathFound==true)
                    break;
            }
            if(pathFound==false) //If no paths are found
                System.out.println("Wavelength cannot be assigned");
//            System.out.println(yenAlg.getResultList().size());
        }
    }
}
