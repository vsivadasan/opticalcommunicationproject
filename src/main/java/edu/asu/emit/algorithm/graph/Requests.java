package edu.asu.emit.algorithm.graph;

import edu.asu.emit.algorithm.utils.Pair;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by vishnu on 4/27/16.
 */
public class Requests {
    // arrival-demand map
    public Map<Integer,Pair<Integer, Integer>> Demand =
            new HashMap<Integer,Pair<Integer, Integer>>();
    // the number of demands
    protected int DemandNum = 0;
    public Requests(final String dataFileName) {
        importFromFile(dataFileName);
    }
    public int length(){
        return Demand.size();
    }
    public void importFromFile(final String dataFileName) {
        Integer index=1;
        // 0. Clear the variables


        try	{
            // 1. read the file and put the content in the buffer
            FileReader input = new FileReader(dataFileName);
            BufferedReader bufRead = new BufferedReader(input);

            boolean isFirstLine = true;
            String line; 	// String that holds current file line

            // 2. Read first line
            line = bufRead.readLine();
            while (line != null) {
                // 2.1 skip the empty line
                if (line.trim().equals("")) {
                    line = bufRead.readLine();
                    continue;
                }

                // get the number of demands
                if (isFirstLine) {
                    //2.2.1 obtain the number of nodes in the graph
                    isFirstLine = false;
                    DemandNum = Integer.parseInt(line.trim());

                } else {
                // store the demands
                    String[] strList = line.trim().split("\\s");


                    int StartNode = Integer.parseInt(strList[0]);
                    int EndNode = Integer.parseInt(strList[1]);
                    Demand.put(index,new Pair<Integer,Integer>(StartNode,EndNode));
                    index+=1;
                }
                //
                line = bufRead.readLine();
            }
            bufRead.close();

        } catch (IOException e) {
            // If another exception is generated, print a stack trace
            e.printStackTrace();
        }
    }

}
