/*
 *
 * Copyright (c) 2004-2008 Arizona State University.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ARIZONA STATE UNIVERSITY ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL ARIZONA STATE UNIVERSITY
 * NOR ITS EMPLOYEES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
package edu.asu.emit.algorithm.graph;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import edu.asu.emit.algorithm.graph.abstraction.BaseElementWithWeight;
import edu.asu.emit.algorithm.graph.abstraction.BaseVertex;
import edu.asu.emit.algorithm.utils.Pair;

/**
 * The class defines a path in graph.
 * 
 * @author yqi
 */
public class Path implements BaseElementWithWeight {
	
	private List<BaseVertex> vertexList = new Vector<BaseVertex>();
	private double weight = -1;
	
	public Path() { }
	
	public Path(List<BaseVertex> vertexList, double weight) {
		this.vertexList = vertexList;
		this.weight = weight;
	}

	public double getWeight() {
		return weight;
	}
	public int numberOfNodes(){
		return this.vertexList.size();
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public List<BaseVertex> getVertexList() {
		return vertexList;
	}
	
	@Override
	public boolean equals(Object right) {
		
		if (right instanceof Path) {
			Path rPath = (Path) right;
			return vertexList.equals(rPath.vertexList);
		}
		return false;
	}

	@Override
	public int hashCode() {
		return vertexList.hashCode();
	}


	public String toString() {
		return vertexList.toString() + ":" + weight;
	}

	//Function to return the edges in the path
	public List<Pair<Integer,Integer>> getEdges(){
		List<Pair<Integer,Integer>> edgeList=new ArrayList<Pair<Integer,Integer>>();
		for (int count=0;count<this.numberOfNodes()-1;count++){
			Pair<Integer,Integer> newPair=new Pair(this.getVertexList().get(count),this.getVertexList().get(count+1));
			edgeList.add(newPair);
		}
		return edgeList;
	}

	//Function to check if any edges overlap (can be improved)

	public boolean checkOverlappingEdges(Path comparingPath){
		List<Pair<Integer,Integer>> List1=this.getEdges();
		List<Pair<Integer,Integer>> List2=comparingPath.getEdges();
		for (int count=0;count<List1.size();count++){
			for (int innerCounter=0;innerCounter<List2.size();innerCounter++)
			{
				if((List1.get(count).first()==List2.get(innerCounter).first())&&(List1.get(count).second()==List2.get(innerCounter).second()))
					return true;
			}
		}
		return false;
	}
}
