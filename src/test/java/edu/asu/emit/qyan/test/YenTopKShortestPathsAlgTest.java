/*
 *
 * Copyright (c) 2004-2008 Arizona State University.  All rights
 * reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY ARIZONA STATE UNIVERSITY ``AS IS'' AND
 * ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL ARIZONA STATE UNIVERSITY
 * NOR ITS EMPLOYEES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package edu.asu.emit.qyan.test;

import edu.asu.emit.algorithm.graph.*;
import edu.asu.emit.algorithm.graph.shortestpaths.DijkstraShortestPathAlg;
import edu.asu.emit.algorithm.graph.shortestpaths.YenTopKShortestPathsAlg;
import org.testng.annotations.Test;

import java.util.List;

/**
 * TODO Need to redo!
 * @version $Revision: 784 $
 * @latest $Id: YenTopKShortestPathsAlgTest.java 46 2010-06-05 07:54:27Z yan.qi.asu $
 */
public class YenTopKShortestPathsAlgTest {
	// The graph should be initiated only once to guarantee the correspondence 
	// between vertex id and node id in input text file. 
	private static Graph graph = new VariableGraph("data/orderDemo");
	//Demand List
	private static Requests demands=new Requests("data/demand1");
	
//	@Test
	public void testDijkstraShortestPathAlg()
	{
		System.out.println("Testing Dijkstra Shortest Path Algorithm!");
		DijkstraShortestPathAlg alg = new DijkstraShortestPathAlg(graph);
		System.out.println(alg.getShortestPath(graph.getVertex(4), graph.getVertex(5)));
	}
	
//	@Test
	public void testYenShortestPathsAlg()
	{		
		System.out.println("Testing batch processing of top-k shortest paths!");
		YenTopKShortestPathsAlg yenAlg = new YenTopKShortestPathsAlg(graph);
		List<Path> shortest_paths_list = yenAlg.getShortestPaths(
                graph.getVertex(4), graph.getVertex(5), 100);
		System.out.println(":"+shortest_paths_list);
		System.out.println(yenAlg.getResultList().size());
	}
		@Test
	public void testMultipleWavelengths()
	{
		System.out.println("Wavelength demand ");
		Graph graph = new VariableGraph("data/myTopology");
		Requests demands=new Requests("data/demand");
		RoutingAndWavelengthAssignment RWA=new RoutingAndWavelengthAssignment(graph,demands);
		System.out.println("Using 1 wavelength");
		RWA.FirstFitAssignment(1);
		System.out.println("Using 2 wavelengths");
		RWA.FirstFitAssignment(2);
		System.out.println("Using 3 wavelengths");
		RWA.FirstFitAssignment(3);
	}
	@Test
	public void testArrivalOrderOfDemands()
	{
		Graph graph = new VariableGraph("data/orderDemo");
		Requests demand1=new Requests("data/demand1");
		Requests demand2=new Requests("data/demand2");
		RoutingAndWavelengthAssignment RWA=new RoutingAndWavelengthAssignment(graph,demand1);
		RoutingAndWavelengthAssignment RWA1=new RoutingAndWavelengthAssignment(graph,demand2);
		System.out.println("First demand :");
		RWA.FirstFitAssignment(1);
		System.out.println("Second demand :");
		RWA1.FirstFitAssignment(1);
	}
	
//	@Test
	public void testYenShortestPathsAlg2()
	{
		System.out.println("Obtain all paths in increasing order! - updated!");
		YenTopKShortestPathsAlg yenAlg = new YenTopKShortestPathsAlg(
				graph, graph.getVertex(4), graph.getVertex(5));
		int i=0;
		while(yenAlg.hasNext())
		{
			System.out.println("Path "+i+++" : "+yenAlg.next());
		}
		
		System.out.println("Result # :"+i);
		System.out.println("Candidate # :"+yenAlg.getCadidateSize());
		System.out.println("All generated : "+yenAlg.getGeneratedPathSize());
	}
	
//	@Test
	public void testYenShortestPathsAlg4MultipleGraphs()
	{
		System.out.println("Graph 1 - ");
		YenTopKShortestPathsAlg yenAlg = new YenTopKShortestPathsAlg(
				graph, graph.getVertex(4), graph.getVertex(5));
		int i=0;
		while(yenAlg.hasNext())
		{
			System.out.println("Path "+i+++" : "+yenAlg.next());
		}
		
		System.out.println("Result # :"+i);
		System.out.println("Candidate # :"+yenAlg.getCadidateSize());
		System.out.println("All generated : "+yenAlg.getGeneratedPathSize());
		
		///
		System.out.println("Graph 2 - ");
		graph = new VariableGraph("data/test_6_1");
		YenTopKShortestPathsAlg yenAlg1 = new YenTopKShortestPathsAlg(graph);
		List<Path> shortest_paths_list = yenAlg1.getShortestPaths(
                graph.getVertex(4), graph.getVertex(5), 100);
		System.out.println(":"+shortest_paths_list);
		System.out.println(yenAlg1.getResultList().size());
	}
}
